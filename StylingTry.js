import React, {Component} from 'react';
import {Text, View, Image, TextInput, StyleSheet, ScrollView} from 'react-native';
import Case from './Images/case.jpeg';
import SampleComponent from './SampleComponent.js'

const Try = () => {
    return(
      <View style={styles.box}>
        <Image source={Case} style={{width: 188, height: 107, borderRadius: 10}}/>
        <Text style={styles.text}>Fruti-Loops MacBook Case</Text>
        <Text style={styles.harga}>Rp 315.000</Text>
        <Text style={styles.alamat}>Jakarta Utara</Text>
        <View style={styles.button}>
          <Text style={{color: 'white', fontWeight: '600', fontSize: 14, textAlign: 'center'}}>BELI</Text>
        </View>
      </View>
    )
  }
  
  const styles = StyleSheet.create({
    text: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: 16,
    },
    harga:{
      color: '#f0932b',
      fontWeight: 'bold',
      fontSize: 12,
      marginTop: 12
    },
    alamat: {
      fontSize: 12,
      marginTop: 12,
      fontWeight: '300'
    },
    box: {
      width: 212,
      height: 280,
      backgroundColor: '#f2f2f2',
      padding: 12,
      margin: 30,
      borderRadius: 10,
      elevation: 5
    },
    button: {
      backgroundColor: '#2ed573',
      paddingTop: 6,
      paddingBottom: 6,
      borderRadius: 25,
      marginTop: 20
    }
  })

  export default Try;