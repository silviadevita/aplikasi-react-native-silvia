import React, {Component} from 'react';
import {Text, View, Image, TextInput, StyleSheet, ScrollView} from 'react-native';
import Case from './Images/case.jpeg';
import SampleComponent from './SampleComponent.js';
import Try from './StylingTry.js'

const App = ()=> {
  return (
    <View>
      <ScrollView>
      <SampleComponent/>
      <Try/>
      </ScrollView>
    </View>
  )
}




export default App;