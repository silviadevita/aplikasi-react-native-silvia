import React, {Component} from 'react';
import {View, Text, TextInput, Image, StyleSheet} from 'react-native';

const SampleComponent = () => {
    return(
      <View style={{margin: 20}}>
        <Text style={{fontSize: 30, textAlign: 'center', marginBottom: 15, fontWeight: 'bold'}}>CONTOH STYLE REACT NATIVE</Text>
        <View style={{alignItems:'center'}}>
          <Photo/>
          <Text style={{marginTop: 10}}>Silvia <Devita/></Text>
          <Text style={styles.text2}>silviadevita09@gmail.com</Text>
        </View>
        <TextInput style={{borderWidth: 1, marginTop:10, borderRadius: 5}} placeholder="Type here.."/>
        <BoxGreen/>
      </View>
    )
}

const Devita = () =>{
    return (
      <Text>Devita</Text>
    )
  }
  
  const Photo = () => {
    return(
      <Image source={{uri: 'http://placeimg.com/100/100/people'}} style={{width: 100, height:100, borderRadius: 100}}/>
    )
  }
  
  
  
  class BoxGreen extends Component{
    render(){
      return(
        <View style={{width: 200, height:50, backgroundColor: '#27ae60', marginTop: 15, borderRadius: 5, alignContent: 'center', justifyContent:'center', alignItems: 'center'}}>
        <Text>This component from class</Text>
        </View>
      )
    }
  }

  const styles = StyleSheet.create({
    text2:{
        fontSize: 14,
        color: 'blue'
      },
  })

  export default SampleComponent